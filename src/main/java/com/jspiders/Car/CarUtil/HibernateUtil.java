package com.jspiders.Car.CarUtil;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil 
{
	private static SessionFactory sf = null;
	private HibernateUtil()
	{
		
	}
	public static SessionFactory getSessionFactory()
	{
		if(sf == null)
		{
			Configuration configuration = new Configuration();
			configuration.configure();
			sf = configuration.buildSessionFactory();
		}
		return sf;
	}

}

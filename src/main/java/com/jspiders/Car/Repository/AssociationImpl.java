package com.jspiders.Car.Repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.jspiders.Car.CarUtil.HibernateUtil;
import com.jspiders.Car.Entity.Car;

public class AssociationImpl implements Association
{

	@Override
	public void saveCarDetails(Car car) 
	{	
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(car);
		transaction.commit();
		System.out.println("Save Successfull");
	}
	

}

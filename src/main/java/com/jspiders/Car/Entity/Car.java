package com.jspiders.Car.Entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.Car.AppConstants.AppConstants;

@Entity
@Table(name = AppConstants.CAR_DETAILS)
public class Car implements Serializable
{
	@Id
	@GenericGenerator(name = "c_auto", strategy = "increment")
	@GeneratedValue(generator = "c_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "car_name")
	private String carName;
	
	@Column(name = "body_type")
	private String bodyType;
	
	@Column(name = "fuel_type")
	private String fuelType;
	
	@Column(name = "engine_CC")
	private String engineDisplacement;
	
	@Column(name = "number_of_seats")
	private Long numberOfSeats;
	
	@Column(name = "mileage_in_kmpl")
	private Long mileage;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fr_key")
	private Brand brand;
	
	public Car() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getBodyType() {
		return bodyType;
	}

	public void setBodyType(String bodyType) {
		this.bodyType = bodyType;
	}

	public String getFuelType() {
		return fuelType;
	}

	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getEngineDisplacement() {
		return engineDisplacement;
	}

	public void setEngineDisplacement(String engineDisplacement) {
		this.engineDisplacement = engineDisplacement;
	}

	public Long getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(Long numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public Long getMileage() {
		return mileage;
	}

	public void setMileage(Long mileage) {
		this.mileage = mileage;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	@Override
	public String toString() {
		return "Car [id=" + id + ", carName=" + carName + ", bodyType=" + bodyType + ", fuelType=" + fuelType
				+ ", engineDisplacement=" + engineDisplacement + ", numberOfSeats=" + numberOfSeats + ", mileage="
				+ mileage + ", brand=" + brand + "]";
	}
	
	
	

}

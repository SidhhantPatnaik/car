package com.jspiders.Car.Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspiders.Car.AppConstants.AppConstants;

@Entity
@Table(name = AppConstants.BRAND_DETAILS)
public class Brand implements Serializable
{
	@Id
	@GenericGenerator(name = "b_auto", strategy = "increment")
	@GeneratedValue(generator = "b_auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "brand_name")
	private String brandName;
	
	@Column(name = "established_year")
	private String establishedYear;
	
	@Column(name = "head_quaters")
	private String headQuaters;
	
	public Brand() {
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getEstablishedYear() {
		return establishedYear;
	}

	public void setEstablishedYear(String establishedYear) {
		this.establishedYear = establishedYear;
	}

	public String getHeadQuaters() {
		return headQuaters;
	}

	public void setHeadQuaters(String headQuaters) {
		this.headQuaters = headQuaters;
	}

	@Override
	public String toString() {
		return "Brand [id=" + id + ", brandName=" + brandName + ", establishedYear=" + establishedYear
				+ ", headQuaters=" + headQuaters + "]";
	}
	
	
	

}

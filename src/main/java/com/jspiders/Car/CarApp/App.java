package com.jspiders.Car.CarApp;

import com.jspiders.Car.Entity.Brand;
import com.jspiders.Car.Entity.Car;
import com.jspiders.Car.Repository.AssociationImpl;

public class App 
{
    public static void main( String[] args )
    {
        Brand brand1 = new Brand();
        
        brand1.setBrandName("Mahindra");
        brand1.setEstablishedYear("1945");
        brand1.setHeadQuaters("Mumbai,India");
        
        Brand brand2 = new Brand();
        
        brand2.setBrandName("Maruti");
        brand2.setEstablishedYear("1966");
        brand2.setHeadQuaters("Delhi,India");
        
        Car car1 = new Car();
        car1.setCarName("Thar");
        car1.setBodyType("SUV");
        car1.setFuelType("petrol/diesel");
        car1.setEngineDisplacement("1997,2184");
        car1.setNumberOfSeats(4L);
        car1.setMileage(14L);
        
        car1.setBrand(brand1);
        
        Car car2 = new Car();
        car2.setCarName("Alto");
        car2.setBodyType("hatchback");
        car2.setFuelType("petrol");
        car2.setEngineDisplacement("1500");
        car2.setNumberOfSeats(5L);
        car2.setMileage(21L);
        
        car2.setBrand(brand2);
        
        AssociationImpl associationImpl = new AssociationImpl();
        associationImpl.saveCarDetails(car1);
        associationImpl.saveCarDetails(car2);
        
       
        
    }
}
